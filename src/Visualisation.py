import matplotlib.pyplot as plt
import networkx as nx
from datetime import datetime, timedelta
from adjustText import adjust_text
import matplotlib.pyplot as plt, mpld3
import numpy as np
from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
import matplotlib.image as mpimg
from PIL import Image
import requests
from io import BytesIO
import warnings
warnings.filterwarnings("ignore")

import progressbar
import svgutils.compose as sc
from IPython.display import SVG


def navigation_number_to_name(nav,traj):
    return str(nav).split('/')[-1].split('-')[-1] if str(nav).split('/')[-1] in [ 'tab-accueil', 'tab-plan', 'plan-oeuvre','oeuvre-detail','parcours-details' ] else str(traj.get_name(int(str(nav).split('/')[-1].split('-')[-1]),traj.publication)).split(' ')[0]

def get_image_publication(nav,traj):
    oeuvre_dict = traj.publication['contenu']['Fr']['oeuvres']
    id_oeuvre = int(str(nav).split('/')[-1].split('-')[-1])
    return [oeuvre_dict[i]['imagePrincipale']['imageDetail']['titre'] for i in range(0,len(oeuvre_dict)) if int(oeuvre_dict[i]['id']) == id_oeuvre][0]

class Visualisation:
    '''
    Visualisation sous forme de séquence

    =================================
    |       Sequence visualisation  |
    =================================

    '''

    def plot_sequence(traj,save,show):
        fig,ax = plt.subplots()
        # Set size
        fig.set_figheight(20)
        fig.set_figwidth(100)


        shared_args = {"weight":"bold", "fontsize" : 12}

        #ax.annotate("Navigation dans \n l'application", (2,2), fontsize=8)
        ax.set_ylim([-0.5, 5])

        texts = []
        li_temp = []
        y = -1

        li_navigation_temp = []
        for i in range(len(traj.li_navigation)-2):
            # Si moins de 30 secondes : On n'affiche pas
            if (traj.li_navigation[i+1][1]-traj.li_navigation[i][1]).total_seconds() < 60 : 
                if not True in [
                (
                    (
                        (trajectory[1] <= traj.li_navigation[i][1] <= trajectory[2]) or 
                        (traj.li_navigation[i][1] <= trajectory[1] <= traj.li_navigation[i+1][1])
                    ) and str(traj.get_id_oeuvre(trajectory[0],traj.publication)) == str(traj.li_navigation[i][0].split('/')[-1])
                )
                for trajectory in traj.li_trajectory
            ] : continue
            li_navigation_temp.append(traj.li_navigation[i])

        for trajectory in traj.li_trajectory:
            # Si moins de 30 secondes : On n'affiche pas
            if (trajectory[2]-trajectory[1]).total_seconds() < 30 : 
                if not True in [
                (
                    ( 
                        (trajectory[1] <= traj.li_navigation[i][1] <= trajectory[2]) or 
                        (traj.li_navigation[i][1] <= trajectory[1] <= traj.li_navigation[i+1][1])
                    ) and str(traj.get_id_oeuvre(trajectory[0],traj.publication)) == str(traj.li_navigation[i][0].split('/')[-1]) 
                )
                for i in range(len(traj.li_navigation)-2)
            ] : continue

            # ================================================== Fill between =====================================
            same_time = [
                (
                    plt.plot( [ max(li_navigation_temp[i][1],trajectory[1]), min(li_navigation_temp[i+1][1],trajectory[2]) ], [ 2.5, 2.5 ], linewidth=2, alpha = 1, c='#46c821',zorder=5),
                    plt.plot( [ max(li_navigation_temp[i][1],trajectory[1]), min(li_navigation_temp[i+1][1],trajectory[2]) ], [ 1, 1 ], linewidth=2, alpha = 1, c='#46c821',zorder=5),
                    plt.plot( [ max(li_navigation_temp[i][1],trajectory[1]), min(li_navigation_temp[i+1][1],trajectory[2]) ], [ 2.5, 2.5 ], linewidth=6, alpha = 0.2, c='#46c821',zorder=5),
                    plt.plot( [ max(li_navigation_temp[i][1],trajectory[1]), min(li_navigation_temp[i+1][1],trajectory[2]) ], [ 1, 1 ], linewidth=6, alpha = 0.2, c='#46c821',zorder=5),
                    ax.fill_between( 
                        [ min(li_navigation_temp[i+1][1],trajectory[2]), max(li_navigation_temp[i][1],trajectory[1])],
                        [ 2.5, 2.5], [1,1],
                        color='#46c821',
                        alpha=.4
                    )
                )
                for i in range(len(li_navigation_temp)-2)
                if (
                    ( 
                        (trajectory[1] <= li_navigation_temp[i][1] <= trajectory[2]) or 
                        (li_navigation_temp[i][1] <= trajectory[1] <= li_navigation_temp[i+1][1])
                    ) and str(traj.get_id_oeuvre(trajectory[0],traj.publication)) == str(li_navigation_temp[i][0].split('/')[-1]) 
                )
            ]
            # ==================================================================================================

            # ================================================= AFTER ==========================================

            if not same_time : [
                (
                    plt.plot( [ 
                        li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2 - timedelta(seconds=10), 
                        li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2 - timedelta(seconds=10)
                        ], [ 2.5, 2.5 ], linewidth=2, alpha = 1, c='#ffa500',zorder=5),
                    plt.plot( [ 
                        trajectory[1] + (trajectory[2] - trajectory[1])/2 - timedelta(seconds=10),
                        trajectory[1] + (trajectory[2] - trajectory[1])/2 + timedelta(seconds=10)
                        ], [ 1, 1 ], linewidth=2, alpha = 1, c='#ffa500',zorder=5),
                    plt.plot( [ 
                        li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2 - timedelta(seconds=10), 
                        li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2 + timedelta(seconds=10)
                        ], [ 2.5, 2.5 ], linewidth=6, alpha = 0.2, c='#ffa500',zorder=5),
                    plt.plot( [ 
                        trajectory[1] + (trajectory[2] - trajectory[1])/2 - timedelta(seconds=10), 
                        trajectory[1] + (trajectory[2] - trajectory[1])/2 + timedelta(seconds=10) 
                        ], [ 1, 1 ], linewidth=6, alpha = 0.2, c='#ffa500',zorder=5),
                    plt.plot( 
                        [ 
                            trajectory[1] + (trajectory[2] - trajectory[1])/2,
                            trajectory[1] + (trajectory[2] - trajectory[1])/2, 
                            li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2,
                            li_navigation_temp[i][1] + (li_navigation_temp[i+1][1] - li_navigation_temp[i][1])/2
                        ], 
                        [ 1, 1.75, 1.75, 2.5 ],
                        c='#ffa500', marker='o', linestyle='dashed')
                )
                for i in range(len(li_navigation_temp)-2)
                if (
                    ( 
                        ( 0 <= (trajectory[2] - li_navigation_temp[i][1]).total_seconds() <= 1200 )
                    ) and str(traj.get_id_oeuvre(trajectory[0],traj.publication)) == str(li_navigation_temp[i][0].split('/')[-1]) 
                )
            ]

            # ==================================================================================================


            # ================================================== PLOT ==========================================
            plt.plot( [ trajectory[1],trajectory[2] ], [ 1, 1 ], c='black')
            plt.plot( [ trajectory[1],trajectory[1] ], [ 0.95, 1.05 ], c='black')
            plt.plot( [ trajectory[2],trajectory[2] ], [ 0.95, 1.05 ], c='black')
            texts.append(plt.text(  trajectory[1], 1.3,str(traj.id_to_name(trajectory[0],traj.publication)).split(' ')[0]+' ('+str(trajectory[0])+')',
                **shared_args
                ))

            # ======================================== PLOT TRANSITION ===================================
            li_temp.append(trajectory)
            y += 1
            if y > 0 :
                plt.plot( [ li_temp[y-1][2],li_temp[y][1] ], [ 0.75, 0.75 ], c='blue')
                plt.plot( [ li_temp[y-1][2],li_temp[y-1][2] ], [ 0.70, 0.80 ], c='blue')
                plt.plot( [ li_temp[y][1],li_temp[y][1] ], [ 0.70, 0.80 ], c='blue')
                texts.append(plt.text(  li_temp[y-1][2] + (li_temp[y][1]-li_temp[y-1][2])/2, 0.60, nx.dijkstra_path(traj.graph, li_temp[y-1][0], li_temp[y][0]),
                    **shared_args
                    ))
            # ==================================================================================================

        for i in range(len(li_navigation_temp)-2):
            # ================================================== PLOT ==========================================
            plt.plot( [ li_navigation_temp[i][1],li_navigation_temp[i+1][1] ], [ 2.5, 2.5 ], c='black')
            plt.plot( [ li_navigation_temp[i][1],li_navigation_temp[i][1] ], [ 2.45, 2.45 ], c='black')
            plt.plot( [ li_navigation_temp[i+1][1],li_navigation_temp[i+1][1] ], [ 2.45, 2.55 ], c='black')
            name = str(li_navigation_temp[i][0]).split('/')[-1]
            name = name.split('-')[-1] if name in [ 'tab-accueil', 'tab-plan', 'plan-oeuvre','oeuvre-detail','parcours-details' ] else str(traj.get_name(int(name),traj.publication)).split(' ')[0]
            texts.append(plt.text(li_navigation_temp[i][1], 2.8, name,
            **shared_args
            ))
            
        # ============================================================================================================
        # ========================================================= DRAW TIMELINE ====================================
        li_timeline = [traj.li_trajectory[0][1], traj.li_trajectory[-1][2]]
        i = li_timeline[0] + timedelta(minutes=5)
        while i < traj.li_trajectory[-1][2]:
            li_timeline.append(i) 
            i = i + timedelta(minutes=5)

        plt.plot( [ li_timeline[0], traj.li_trajectory[-1][2] ], [ 0,0 ], c='#4e4eff')
        [texts.append(plt.text( time, 0.05, str(time.hour)+' : '+str(time.minute), **shared_args)) for time in li_timeline]
        [plt.plot( [time, time ], [5, -5], c= '#cecece',zorder=-1, alpha=0.5) for time in li_timeline]
        adjust_text(texts)


        plt.axis('off')
        ax.grid(True, alpha=0.3)

        if show : plt.show()
        if save : mpld3.save_html(fig,save)

        return fig, ax

# ============================================================================================================
# ============================================================================================================

    '''
    Visualisation sous forme de séquence

    =================================
    |    Visualisation Rond         |
    =================================

    '''

    def round_visu(traj,dict_etage,dict_color):
        G = nx.DiGraph(directed=True)
        G.add_nodes_from([module[0] for module in traj.li_trajectory])


        labels = {}
        for i in range(len(traj.li_trajectory)-1) :
            G.add_edge(traj.li_trajectory[i][0],traj.li_trajectory[i+1][0])
            labels[(traj.li_trajectory[i][0], traj.li_trajectory[i+1][0])] = '{}'.format(traj.li_trajectory[i][0])

        cmap = [ dict_color[dict_etage[node]] for node in G ]
        cmap_edge = [ dict_color[dict_etage[u[0]]] for u in G.edges() ]

        fig= plt.figure(figsize=(10,10))

        options = {
            'node_color': cmap,
            'node_size': 850,
            'width': 1,
            'arrowstyle': '-|>',
            'arrowsize': 15,
        }

        #nx.draw_circular(G, arrows=True, with_labels=True, **options)
        pos = nx.circular_layout(G)

        nx.draw_networkx_nodes(G, 
                                pos, 
                                node_color = cmap, 
                                node_size = 850, 
                                with_label = True
        )
        nx.draw_networkx_edges(
            G, pos,
            connectionstyle="arc3,rad=0.1", 
            edge_color = cmap_edge,
            **options
        )

        nx.draw_networkx_edge_labels(G, pos,edge_labels=labels)
        nx.draw_networkx_labels(G,pos,font_size=12,font_color='w')

        plt.show()

# ============================================================================================================
# ============================================================================================================

    '''
    Visualisation sous forme de séquence animé

    =================================
    |    Visualisation Rond Animé   |
    =================================

    '''

    def round_visu_anim(self,traj):
        G = nx.DiGraph(directed=True)
        cmap = []
        options = {
            'node_color': cmap,
            'node_size': 850,
            'width': 2,
            'arrowstyle': '-|>',
            'arrowsize': 20,
        }
        for i in range(1,len(traj.li_trajectory)) :
            [G.add_node(node) for node in [traj.li_trajectory[i][0],traj.li_trajectory[i-1][0]] if node not in G.nodes() ]
            G.add_edge(traj.li_trajectory[i-1][0],traj.li_trajectory[i][0])

            fig= plt.figure(figsize=(10,10))
            pos = nx.circular_layout(G)
            cmap = ['#ff2626' if node is traj.li_trajectory[i][0] else '#FFFFFF' for node in G.nodes()]
            cmap_edge = [ '#ff2626' if (u,v) == (traj.li_trajectory[i-1][0],traj.li_trajectory[i][0]) else 'black' for u,v in G.edges() ]
            nx.draw_networkx_nodes(G, 
                                    pos, 
                                    node_color = cmap, 
                                    node_size = 850, 
                                    with_label = True
            ).set_edgecolor('black')
            nx.draw_networkx_edges(
                G, pos,
                connectionstyle="arc3,rad=0.1",
                edge_color = cmap_edge,
                **options
            )
            nx.draw_networkx_labels(G,pos,font_size=12,font_color='#1b1b1b')

            fig.canvas.draw()       # draw the canvas, cache the renderer
            image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
            image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            plt.close

            yield image

# ============================================================================================================
# ============================================================================================================

    '''
    Visualisation sous forme de séquence animé

    =================================
    |    Visualisation Animé Traj   |
    =================================

    '''

    def visu_anim_carte(traj,plan,li_module):
        print('Start map animation ...')

        G = nx.DiGraph(directed=True)


        cmap = []
        options = {
            'node_color': cmap,
            'node_size': 850,
            'arrowstyle': '-|>',
            'arrowsize': 20,
            'alpha' : 0.5,
            'connectionstyle' : "arc3,rad=0.1",
            'edge_color' : "#413E45",
            'width' : 2
        }
        pos = li_module

        widgets=[
            ' [', progressbar.Timer(), '] ',
            progressbar.Bar(),
            ' (', progressbar.ETA(), ') '
        ]

        for i in progressbar.progressbar(range(1,len(traj.li_trajectory)), widgets=widgets) :
            
            # Import the map
            img = plt.imread(plan)
            fig, ax = plt.subplots()
            
            # Set size
            fig.set_figheight(20)
            fig.set_figwidth(20)

            # Hide axis
            plt.axis('off')
            ax.set_ylim(1600,0)
            ax.imshow(img,alpha=0.8)

            navigation = [ traj.li_navigation[y] for y in range(len(traj.li_navigation)) if  (traj.li_trajectory[i-1][1] <= traj.li_navigation[y][1] <= traj.li_trajectory[i][1]) ]
            dict_navigation_compil = {}
            for nav in navigation :
                dict_navigation_compil[nav[0]] = (nav[1] - traj.li_trajectory[i-1][1]) if nav[0] not in dict_navigation_compil.keys() else dict_navigation_compil[nav[0]]  + (nav[1] - traj.li_trajectory[i-1][1])
            
            dict_image = {
                'tab-accueil' : 'data/Accueil.PNG',
                'tab-plan' : 'data/plan.PNG',
                'plan-oeuvre' : 'data/plan.PNG',
                '71' : 'data/parcours.PNG'
            }
            navigation = sorted([ (key,dict_navigation_compil[key]) for key in dict_navigation_compil.keys() ], key = lambda x : x[1] )[:3]
            
            if not navigation : 
                img = Image.open('data/no_navigation.png').resize((500, 800))
                ax.add_artist(AnnotationBbox(OffsetImage(img, zoom=0.3), (200,1400), frameon=False))
                ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/phone.png'), zoom=0.3), (200,1400), frameon=False))     

            else : 
                for y,nav in enumerate(navigation) :
                    if str(nav[0]).split('/')[-1] not in [ 'tab-accueil', 'tab-plan', 'plan-oeuvre','oeuvre-detail','parcours-details', '71' ] :
                        url = 'https://data.jpeuxpasjaimusee.org/vm/museum-de-la-rochelle/diffusee/medias/{}'.format(get_image_publication(nav[0],traj))
                        response = requests.get(url)
                        img = Image.open(BytesIO(response.content)).resize((500, 800))
                        ax.add_artist(AnnotationBbox(OffsetImage(img, zoom=0.3), (200+y*600,1400), frameon=False))
                        ax.text(50+y*600, 1750, "{} \n {} \n {}".format(
                            str(nav[0]).split('/')[-2],
                            navigation_number_to_name(nav[0],traj), 
                            nav[1]
                        ),
                            fontsize = 18,
                            bbox=dict(facecolor='#fffaf0', alpha=0.5))
                    else :

                        img = Image.open(dict_image[str(nav[0]).split('/')[-1]]).resize((500, 800))
                        ax.add_artist(AnnotationBbox(OffsetImage(img, zoom=0.3), (200+y*600,1400), frameon=False))
                        ax.text(50+y*600, 1775, "{} \n {} \n {}".format(
                            str(nav[0]).split('/')[-2],
                            navigation_number_to_name(nav[0],traj), 
                            nav[1]
                        ),
                            fontsize = 18,
                            bbox=dict(facecolor='#fffaf0', alpha=0.5))

                [ ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/phone_red.png'), zoom=0.3), (200+y*600,1400), frameon=False)) 
                    if (
                            str(traj.get_id_oeuvre(traj.li_trajectory[i-1][0],traj.publication)) == str(navigation[y][0].split('/')[-1]) 
                        ) else ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/phone.png'), zoom=0.3), (200+y*600,1400), frameon=False)) 
                for y in range(len(navigation)) ]

            [G.add_node(node) for node in [traj.li_trajectory[i][0],traj.li_trajectory[i-1][0]] if node not in G.nodes() ]
            
            G.add_edge(traj.li_trajectory[i-1][0],traj.li_trajectory[i][0])

            if True in [ ( str(traj.get_id_oeuvre(traj.li_trajectory[i-1][0],traj.publication)) == str(navigation[y][0].split('/')[-1]) ) for y in range(len(navigation)) ] :
                ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/standing_match.png'), zoom=0.12), pos[traj.li_trajectory[i-1][0]], frameon=False)) if (traj.li_trajectory[i][1] - traj.li_trajectory[i-1][1]).total_seconds() > 60 else ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/running_match.png'), zoom=0.12), pos[traj.li_trajectory[i-1][0]], frameon=False))
            else :
                ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/standing.png'), zoom=0.12), pos[traj.li_trajectory[i-1][0]], frameon=False)) if (traj.li_trajectory[i][1] - traj.li_trajectory[i-1][1]).total_seconds() > 60 else ax.add_artist(AnnotationBbox(OffsetImage(mpimg.imread('data/running.png'), zoom=0.12), pos[traj.li_trajectory[i-1][0]], frameon=False))
            
            ax.set_title('{}h : {}m -> {}h : {}m'.format(
                traj.li_trajectory[i-1][1].hour,
                traj.li_trajectory[i-1][1].minute,
                traj.li_trajectory[i][1].hour,
                traj.li_trajectory[i][1].minute),loc='center',fontsize=25)
            nx.draw_networkx_nodes(G, 
                                    pos, 
                                    node_color = "#413E45", 
                                    node_size = 850
            ).set_edgecolor('#413E45')

            nx.draw_networkx_edges(
                G, pos,
                **options
            )
            #nx.draw_networkx_labels(G,pos,font_size=12,font_color='#1b1b1b')

            ax.text(600, 1850, "Utilisation de l'application",
                fontsize = 30,
                bbox=dict(facecolor='#e60000', alpha=0.5))

            fig.canvas.draw()       # draw the canvas, cache the renderer
            image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
            image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))

            plt.close()
            plt.cla()
            plt.clf()
            
            yield image
        print('Done ! ')

# ============================================================================================================
# ============================================================================================================

    '''
    Visualisation sous forme de séquence animé

    =================================
    |    Visualisation Animé Traj   |
    =================================

    '''

    def sequence_anim(traj):
        print('Start sequence animation ...')
        fig, ax = Visualisation.plot_sequence(traj,save=False,show=False)
        for i in progressbar.progressbar(range(1,len(traj.li_trajectory))) :
            ax.set_xlim(traj.li_trajectory[i-1][1],traj.li_trajectory[i][1])
            fig.canvas.draw()       # draw the canvas, cache the renderer
            image = np.frombuffer(fig.canvas.tostring_rgb(), dtype='uint8')
            image  = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            plt.close

            yield image