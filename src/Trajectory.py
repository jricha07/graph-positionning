import json
from os import path
import pandas as pd
from pprint import pprint
import matplotlib.pyplot as plt
from datetime import datetime, timedelta
import networkx as nx
import json
import math
from adjustText import adjust_text
import matplotlib.pyplot as plt, mpld3
import imageio

from src.Visualisation import Visualisation as visu

class Trajectory :
    def __init__(self, id_user, beacon_file, navigation_file, publication_file, li_node, li_edge):
        self.id_user = id_user
        beacon_dict = self.get_beacons(pd.DataFrame(json.load(beacon_file)),self.id_user)
        self.li_navigation = self.merge_dict(self.get_navigation(pd.DataFrame(json.load(navigation_file)),self.id_user))

        self.li_RSSI = []
        [ 
            [ 
                self.li_RSSI.append( 
                (datetime.strptime(rssi_value['timeStamp'].split('T')[1],"%H:%M:%S"),
                rssi_value['minor'],
                rssi_value['RSSI']) 
                ) 
                    for rssi_value in  json.loads(beacon_dict[beacon])
                if rssi_value['RSSI'] > -85 
            ] 
        for beacon in beacon_dict
        ]

        # Graph generating
        self.graph = nx.Graph()
        [ self.graph.add_node(module_nb) for module_nb in li_node ]
        [ self.graph.add_edge(edge[0],edge[1]) for edge in li_edge ]

        # Get navigation dict
        self.publication = json.load(publication_file)
        self.li_trajectory = self.get_trajectory_graph(10) if self.li_RSSI else print('sequence is empty')

    def get_beacons(self,df,id_user):
        select_user = df.query("userId == "+id_user)
        return select_user['beacons'].to_dict()

    def get_navigation(self,df,id_user):
        select_user = df.query("userId == "+id_user)
        return select_user[['currentPage', 'timeStamp']].to_dict()

    # =================================================================================================
    # ================================ Data Processing ================================================

    def daterange(self,window_size,start_time,end_time):
        for n in range(window_size,int((end_time - start_time).total_seconds()),window_size):
            yield (start_time + timedelta(seconds=n-window_size),start_time + timedelta(seconds=n))

    def sliding_window(self,window_size,start_time,end_time):
        li_pos = []
        for single_date in self.daterange(window_size,start_time,end_time):
            li_value = []
            for value in self.li_RSSI:
                if single_date[0] <= ( value[0] ) and single_date[1] >= ( value[0] ): li_value.append(value)
            if li_value : li_pos.append(li_value)
        return li_pos
        
    # ================================================================================================
    # ======================= Concaténation des séquences ============================================
    # Ex : [ (a, ti_1, te_1), (a,ti_2, te_2)] ==> [ (b, ti_1, te_2) ]
    def reduce_sequence(self,li_raw_sequence):
        li_trajectory = []
        i = 1
        while i < len(li_raw_sequence) - 1:
            li_temp = [li_raw_sequence[i]]

            while li_raw_sequence[i][0] == li_raw_sequence[i+1][0]:
                li_temp.append(li_raw_sequence[i+1])
                i += 1
                if i >= len(li_raw_sequence)-1 : break

                
            if li_temp : li_trajectory.append( ( li_temp[0][0], li_temp[0][1], li_temp[-1][1] ) )
            i+=1
        return li_trajectory

    # ================================================================================================
    # ============================== Récupération de la trajectoire ==================================
    # Get sequence of module depending on a time window using a non-directive graph
    # NON OPTIMIZED : FUSE THE TWO LOOP
    def get_trajectory_graph(self,window_size):
        # Start the trajectory by the first devise
        li_trajectory = [ ( self.li_RSSI[0][1], self.li_RSSI[0][0] ) ]
        li_raw_sequence = [    
                    # Get all the beacon who appear during the time window
                    # The purpuse of the tuple is to distinguish the best ``candidate" to put into the trajectory list
                    # It as 3 factors :
                    #   - The mean value of RSSI during the time window
                    #   - The distance between the current device and the candidate (The closest the better)
                    #   - The number of time we get the device in the raw data list (Value of RSSI may changes between beacons)
                    ([  
                        (module, 
                        sum([ (value[2] if value[1] != 32 else value[2]+20) for value in li_value if value[1] == module])/len([value[2] for value in li_value if value[1] == module]),
                        len(nx.dijkstra_path(self.graph, module, li_trajectory[-1][0])),
                        len([value for value in li_value if value[1] == module])
                        
                        )
                        for module in list(set([value[1] for value in li_value]))
                    ]
                    ,li_value[0][0]) 
                for li_value in self.sliding_window(window_size,self.li_RSSI[0][0],self.li_RSSI[-1][0]) 
                ]
        # Loop over li_raw_dequence
        for value in li_raw_sequence:
            li_candidate = [ (module[0], module[1]) for module in value[0] if module[0] == li_trajectory[-1][0] ] if [ (module[0], module[1]) for module in value[0] if module[0] == li_trajectory[-1][0] ] else []
            for neighboor in self.graph.neighbors(li_trajectory[-1][0]) :
                if neighboor in [ module[0] for module in value[0] ] :
                    li_candidate.append([ (module[0], module[1]) for module in value[0] if module[0] == neighboor ][0])
            if li_candidate : li_trajectory.append( ( max( [ (module[0], module[1]) for module in li_candidate ], key= lambda x : x[1])[0], value[1] ) )
        return self.reduce_sequence(li_trajectory)
    

    # ================================================================================================
    # ============================== Traitement de la navigation =====================================
    def merge_dict(self,navigation_dict):
        li_navigation = []
        for key in navigation_dict['currentPage']:
            li_navigation.append((navigation_dict['currentPage'][key],(datetime.strptime(navigation_dict['timeStamp'][key].split('T')[1],"%H:%M:%S"))))
        return li_navigation
    
    def get_id_oeuvre(self,id_oeuvre,publication):
        balise_dict = publication['contenu']['balises']
        try :
            return [balise_dict[i]['oeuvres_id'] for i in range(0,len(balise_dict)-1) if balise_dict[i]['minor'] == id_oeuvre][0][0]
        except :
            return  None

    def get_name(self,id_oeuvre,publication):
        oeuvre_dict = publication['contenu']['Fr']['oeuvres']
        try :
            return [oeuvre_dict[i]['nom'] for i in range(0,len(oeuvre_dict)) if int(oeuvre_dict[i]['id']) == id_oeuvre][0]
        except :
            return id_oeuvre

    def id_to_name(self,id_oeuvre,publication):
        balise_dict = publication['contenu']['balises']
        oeuvre_dict = publication['contenu']['Fr']['oeuvres']
        try :
            id_balise = [balise_dict[i]['oeuvres_id'] for i in range(0,len(balise_dict)-1) if balise_dict[i]['minor'] == id_oeuvre][0][0]
            name = [oeuvre_dict[i]['nom'] for i in range(0,len(oeuvre_dict)) if int(oeuvre_dict[i]['id']) == id_balise][0]
            return name
        except :
            return 'escalier'

    # ================================================================================================
    # ======================================= Visualisation  =========================================
    def plot_sequence(self,save = False, show = True):
        visu.plot_sequence(self,save,show) if self.li_RSSI else print('sequence is empty')
    
    def round_visu(self,dict_etage,dict_color):
        visu.round_visu(self,dict_etage,dict_color) if self.li_RSSI else print('sequence is empty')
    
    def round_visu_anim(self,path,fps):
        imageio.mimsave(path, visu.round_visu_anim(self), fps=fps, subrectangles = True) if self.li_RSSI else print('sequence is empty')

    def visu_anim_carte(self,plan,li_module,path,fps):
        if self.li_RSSI : imageio.mimsave(path, visu.visu_anim_carte(self,plan,li_module), fps=fps, subrectangles = True)
    
    def sequence_anim(self,path,fps):
        if self.li_RSSI : imageio.mimsave(path, visu.sequence_anim(self), fps=fps, subrectangles = True)  


