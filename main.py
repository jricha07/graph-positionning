from src.Trajectory import Trajectory
import yaml
import networkx
import pydot
import json
import moviepy.editor as mp

li_edges = yaml.safe_load(open('data/edges.yaml'))
li_module = {
    34 : (283,196),
    12 : (160,195),
    2 : (64,362),
    16 : (50,710),
    5 : (96,850),
    33 : (209, 865),
    35 : (384,930),
    36 : (519,800),
    25 : (684,835),
    7 : (706,925),
    28 : (719, 1115),
    20 : (713,632),
    26 : (798,728),
    10 : (879,710),
    8 : (1030,774),
    6 : (1048,655),
    27 : (1100,752),
    3 : (1099,853),
    22 : (1064,1094),
    23 : (1280,700),
    21 : (1403,703),
    31 : (1604,667),
    32 : (1610,870),
    19 : (1109,519),
    15 : (1069,332),
    11 : (1092,130),
    18 : (1209,45),
    17 : (1182,307),
    13 : (1223,506),
    4 : (1397,120),
    14 : (1636,81),
    24 : (1564,221)
}

dict_individuals = {
    "'20210919T091351807'": '9h27',
    "'20210919T09190325'": '9h40',
    "'20210919T091832770'": '9h44',
    "'20210919T092214929'": '9h48',
    "'20210919T10405672'": '10h46',
    "'20210919T105059878'": '10h53',
    "'20210919T112024622'": '11h23',
    "'20210919T113811682'": '11h41',
    "'20210919T111824519'": '11h50',
    "'20210919T113209969'": '11h58',
    "'20210919T120140137'": '12h07',
    "'20210919T114723722'": '12h10',
    "'20210919T12212863'": '12h26',
    "'20210919T122635974'": '12h26',
    "'20210919T132800229'": '13h42',
    "'20210919T134435111'": '13h52',
    "'20210919T134315349'": '13h58',
    "'20210919T132802140'": '14h00',
    "'20210919T133401108'": '14h01',
    "'20210919T141041671'": '14h08',
    "'20210919T144035827'": '14h44',
    "'20210919T145509816'": '15h02',
    "'20210919T14593969'": '15h10',
    "'20210919T152014408'": '15h20',
    "'20210919T16074646'": '16h15',
    "'20210919T144035827'": '16h16',
    "'20210919T160853423'": '16h17',
    "'20210919T163029430'": '16h30',
    "'20210919T163906579'": '16h59',
    "'20210919T164556100'": '16h54',
}

module_etage = {
    2:0,
    12:0,
    34:0,
    16:-1,
    5:-1,
    33:-1,
    35:-1,
    36:-1,
    19:1,
    15:1,
    11:1,
    18:1,
    17:1,
    13:1,
    4:1,
    14:1,
    24:1,
    25:2,
    7:2,
    20:2,
    28:2,
    26:2,
    10:2,
    6:2,
    27:2,
    8:2,
    3:2,
    22:2,
    23:3,
    21:3,
    31:3,
    32:3
}

etage_color =  {
    -1 : "#ff3333",
    0 : "#0080ff",
    1 : "#ff7f00",
    2 : "#5a8300",
    3 : "#7f00ff"
}

traj = Trajectory(
    "'20210919T113811682'", 
    open("data/beacons.json"), 
    open("data/navigation.json"), 
    open("data/publication.json"), 
    [ i for i in range(2,36) if i not in [9,29,30] ], 
    li_edges
)#.plot_sequence(save='result/traj_'+user.replace('\'','')+'.html',show=False)

traj.visu_anim_carte('data\Plan Beacon_carte_only.png',li_module,'anim_carte.gif',1)
clip = mp.VideoFileClip("anim_carte.gif")
clip.write_videofile("anim_carte.mp4")

'''
for individual in dict_individuals:
    traj = Trajectory(
        individual, 
        open("data/beacons.json"), 
        open("data/navigation.json"), 
        open("data/publication.json"), 
        [ i for i in range(2,36) if i not in [9,29,30] ], 
        li_edges
    )
    
    try : 
        traj.visu_anim_carte('data\Plan Beacon_carte_only.png',li_module,'result/animations/animation_{}.gif'.format(individual),1)
    except :
        print('oups, {} couldnt resolve'.format(individual))
'''

